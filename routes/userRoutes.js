const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.param('id', function (req, res, next, id) {
  req.user = UserService.search({ id });
  next();
})

router.route('/')
  .get((req, res, next) => {
    try {
        const users = UserService.list();
        res.data = users;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware)
  .post(createUserValid, (req, res, next) => {
    if (res.err) {
      return next();
    }

    try {
        const user = req.body;
        res.data = UserService.create(user);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware);

router.route('/:id')
  .get((req, res, next) => {
    try {
      if (!req.user) {
        res.status(404);
        throw new Error('User not found');
      }
      res.data = req.user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .put(updateUserValid, (req, res, next) => {
    if (res.err) {
      return next();
    }

    try {
      if (!req.user) {
        res.status(404);
        throw new Error('User not found');
      }
      res.data = UserService.update(req.user.id, req.body);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware)
  .delete((req, res, next) => {
    try {
      if (!req.user) {
        res.status(404);
        throw new Error('User not found');
      } 
      res.data = UserService.delete(req.user.id);
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware);

module.exports = router;