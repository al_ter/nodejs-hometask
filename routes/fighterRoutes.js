const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.param('id', function (req, res, next, id) {
  req.fighter = FighterService.search({ id });
  next();
})

router.route('/')
  .get((req, res, next) => {
    try {
        const fighters = FighterService.list();
        res.data = fighters;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware)
  .post(createFighterValid, (req, res, next) => {
    if (res.err) {
      return next();
    }

    try {
        const fighter = req.body;
        res.data = FighterService.create(fighter);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware);

router.route('/:id')
  .get((req, res, next) => {
    try {
      if (!req.fighter) {
        res.status(404);
        throw new Error('Fighter not found');
      }
      res.data = req.fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .put(updateFighterValid, (req, res, next) => {
    if (res.err) {
      return next();
    }

    try {
      if (!req.fighter) {
        res.status(404);
        throw new Error('Fighter not found');
      }
      res.data = FighterService.update(req.fighter.id, req.body);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware)
  .delete((req, res, next) => {
    if (res.err) {
      return next();
    }

    try {
      if (!req.fighter) {
        res.status(404);
        throw new Error('Fighter not found');
      } 
      res.data = FighterService.delete(req.fighter.id);
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware);

module.exports = router;