const { Router } = require('express');
const FightService = require('../services/fightService');
const { fightValid } = require('../middlewares/fight.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

router.param('id', function (req, res, next, id) {
  req.fight = FightService.search({ id });
  next();
})

router.route('/')
  .get((req, res, next) => {
    try {
        const fights = FightService.list();
        res.data = fights;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware)
  .post(fightValid, (req, res, next) => {
    try {
        const fight = req.body;
        res.data = FightService.create(fight);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware);

router.route('/:id')
  .get((req, res, next) => {
    try {
      if (!req.fight) {
        res.status(404);
        throw new Error('Fight not found');
      }
      res.data = req.fight;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .delete((req, res, next) => {
    try {
      if (!req.fight) {
        res.status(404);
        throw new Error('Fight not found');
      } 
      res.data = FightService.delete(req.fight.id);
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware);

module.exports = router;