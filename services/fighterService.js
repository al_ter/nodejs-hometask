const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  list() {
    const fighters = FighterRepository.getAll();
    if (!fighters) {
      return null;
    }
    return fighters;
  }
  
  delete(fighterId) {
    return FighterRepository.delete(fighterId);
  }

  update(fighterId, fighterData) {
    return FighterRepository.update(fighterId, fighterData);
  }

  create(fighterData) {
    // To win the game, you must kill me, John Romero! ;)
    fighterData.health = fighterData.health ? fighterData.health : 100;

    if (fighterData.name === 'IDDQD') {
      fighterData.health = 9999;
      fighterData.defense = 9999;
    } else if (fighterData.name === 'IDKFA'){
      fighterData.power = 9999;
    }
    return FighterRepository.create(fighterData);
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if(!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FighterService();