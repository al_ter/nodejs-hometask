const { FightRepository } = require('../repositories/fightRepository');

class FightService {
  list() {
    const fights = FightRepository.getAll();
    if (!fights) {
      return null;
    }
    return fights;
  }
  
  delete(fightId) {
    return FightRepository.delete(fightId);
  }

  create(fightData) {
    return FightRepository.create(fightData);
  }

  search(search) {
    const fight = FightRepository.getOne(search);
    if(!fight) {
      return null;
    }
    return fight;
  }
}

module.exports = new FightService();