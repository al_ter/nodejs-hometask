const { UserRepository } = require('../repositories/userRepository');

class UserService {
  list() {
    const users = UserRepository.getAll();
    if (!users) {
      return null;
    }
    return users;
  }
  
  delete(userId) {
    return UserRepository.delete(userId);
  }

  update(userId, userData) {
    return UserRepository.update(userId, userData);
  }

  create(userData) {
    return UserRepository.create(userData);
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if(!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();