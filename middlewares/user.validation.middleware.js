const { user } = require('../models/user');
const UserService = require('../services/userService');
const Joi = require('@hapi/joi');

const userSchema = Joi.object({
  firstName: Joi.string().pattern(/^[a-zA-Z]+$/).min(3).max(30),
  lastName: Joi.string().pattern(/^[a-zA-Z]+$/).min(3).max(30),
  email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ["com"] } } ).pattern(/^[^@]+@gmail\.com$/),
  phoneNumber: Joi.string().pattern(/^\+380(39|67|68|96|97|98|50|66|95|99|63|73|93|91|92|94|70|80|90)[\d]{7}$/),
  password: Joi.string().alphanum().min(3).max(30)
});

const userCreateSchema = userSchema.concat(Joi.object({
  firstName: Joi.required(),
  lastName: Joi.required(),
  email: Joi.required(),
  phoneNumber: Joi.required(),
  password: Joi.required()
}));

const createUserValid = (req, res, next) => {
  try {
    const { error } = userCreateSchema.validate(req.body);
    if (error) {
      throw new Error(`User entity to create isn't valid`);
    }
    const { email, phoneNumber } = req.body;
    if ( UserService.search({ email }) || UserService.search({ phoneNumber }) ) {
      throw new Error(`User already exists`);
    };
  } catch (err) {
    res.status(400);
    res.err = err;
  } finally {
    next();
  }
}

const updateUserValid = (req, res, next) => {
  try {
    const { error } = userSchema.validate(req.body);
    if (error) {
      throw new Error(`User entity to update isn't valid`);
    }
  } catch (err) {
    res.status(400);
    res.err = err;
  } finally {
    next();
  }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;