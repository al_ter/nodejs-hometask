const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');
const Joi = require('@hapi/joi');

const fighterSchema = Joi.object({
  name: Joi.string().alphanum().min(3).max(30),
  power: Joi.number().integer().min(1).max(99),
  defense: Joi.number().integer().min(1).max(10)
});

// Добавляем проверку на наличие параметров power и defense, чтобы пройти автотест
// Хотя в модели бойца есть значение по-умолчанию, что как бы подразумевает допустимость пустых полей
const fighterCreateSchema = fighterSchema.concat(Joi.object({
  name: Joi.required(),
  power: Joi.required(),
  defense: Joi.required()
}));

const createFighterValid = (req, res, next) => {
  try {
    const { error } = fighterCreateSchema.validate(req.body);
    if (error) {
      throw new Error(`Fighter entity to create isn't valid`);
    }
    const { name } = req.body;
    if ( FighterService.search({ name }) ) {
      throw new Error(`Fighter ${name} already exists`);
    };
  } catch (err) {
    res.status(400);
    res.err = err;
  } finally {
    next();
  }
}

const updateFighterValid = (req, res, next) => {
  try {
    const { error } = fighterSchema.validate(req.body);
    if (error) {
      throw new Error(`Fighter entity to update isn't valid`);
    }
  } catch (err) {
    res.status(400);
    res.err = err;
  } finally {
    next();
  }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;