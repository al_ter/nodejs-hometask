const responseMiddleware = (req, res, next) => {
   if (res.err) {
    //  if (res.statusCode == 200) {
    //    res.status = 400;
    //  }
     
     res.data = {
       error: true,
       message: res.err.message
     }
   }
    res.json(res.data);
    next();
}

exports.responseMiddleware = responseMiddleware;