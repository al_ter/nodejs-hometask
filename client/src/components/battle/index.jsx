import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import battleServise from '../../services/battleServise';
import { createFight } from '../../services/domainRequest/fightRequest';

import './battle.css';

const Battle = ({ fighter1, fighter2, toggleRumble }) => {
  const [ state, setState ] = useState({ winner: null, fight: {} });
  const [ saveFight, setSaveFight ] = useState(false);

  const toggleSaveFight = () => {
    const checked = document.getElementById('save').checked;
    setSaveFight(checked);
  }

  const closeHandler = () => {
    if (saveFight) {
      createFight(state.fight);
    }
    toggleRumble();
  }

  useEffect(() => {
    battleServise(fighter1, fighter2)
      .then(result => {
        setState(result);
      });
  }, [fighter1, fighter2]);

  return (
    <>
      <Grid
        container
        spacing={3}
        direction="row"
        justify="center"
        alignItems="center"
        id="battle"
      >
        <Grid item xs={5}>
          <Paper className="fighter fighter1" elevation={3}>
            <div className="health-bar__wrapper">
              <div className="health-bar" />
            </div>
            <div className="fighter-name">{fighter1.name}</div>
          </Paper>
        </Grid>
        <Grid item xs={2}>
          <Paper className="vs-block" elevation={0}>
            VS
          </Paper>
        </Grid>
        <Grid item xs={5}>
          <Paper className="fighter fighter2" elevation={3}>
            <div className="health-bar__wrapper">
              <div className="health-bar" />
            </div>
            <div className="fighter-name">{fighter2.name}</div>
          </Paper>
        </Grid>
      </Grid>
      {state.winner && 
        <Grid container className="result-wrapper">
          <Grid item xs={6} className="result">
            <Paper id="winner" elevation={0}>
              <strong>{state.winner.name}</strong> WINS!
            </Paper>
            <label htmlFor="name">Save fight?</label>
            <input id="save" name="save" type="checkbox" onChange={toggleSaveFight} />
            <Button variant="contained" color="primary" onClick={closeHandler} >
              Close
            </Button>
          </Grid>
        </Grid>
      }
    </>
  );
}

export default Battle;
