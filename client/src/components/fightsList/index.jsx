import React, { useState } from 'react';
import FightListElement from '../fightListElement';
import { List, ListItem } from '@material-ui/core';

const FightsList = ({ fights }) => {
  const [ openedFight, setOpenedFight ] = useState('');
  const toggleOpened = id => openedFight === id ? setOpenedFight('') : setOpenedFight(id);

  return (
    <List component="nav" id="fights">
      {fights.map(fight => (
        <ListItem
          button
          classes={{ root: 'fight-item' }}
          key={fight.id}
          onClick={() => toggleOpened(fight.id)}
        >
          <FightListElement isOpened={fight.id === openedFight} fight={fight} />
        </ListItem>
      ))}
    </List>
  );
}

export default FightsList;
