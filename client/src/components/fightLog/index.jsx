import React from 'react';
import FightLogStep from '../fightLogStep';

const FightLog = ({ log }) => {
  return (
    <div className="fight-log">
      {
        log.map((step, index) => (
          <FightLogStep
            key={`${index} + ${step.fighter1Health} + ${step.fighter2Health} + ${step.fighter1Shot} + ${step.fighter2Shot}`}
            step={step}
          />
        ))
      }
    </div>
  );
}

export default FightLog;
