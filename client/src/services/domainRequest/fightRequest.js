import { get, post } from "../requestHelper";
import { FIGHT_ENTITY } from '../../constants/entities';

const entity = FIGHT_ENTITY;

export const getFights = async () => {
  return await get(entity);
}

export const createFight = async (body) => {
  return await post(entity, body);
}
